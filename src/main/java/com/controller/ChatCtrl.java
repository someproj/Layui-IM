package com.controller;

import com.alibaba.fastjson.JSONObject;
import com.bean.*;
import com.service.ChatmsgService;
import com.service.FriendsService;
import com.service.GroupsService;
import com.service.MineService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
public class ChatCtrl {
    @Autowired
    FriendsService friendsService;
    @Autowired
    MineService mineService;
    @Autowired
    ChatmsgService chatmsgService;
    @Autowired
    GroupsService groupsService;

    /**
     * 上传聊天图片
     * **/
    @PostMapping(value = "/chat/upimg")
    @ResponseBody
    public JSONObject upimg(@RequestParam(value = "file", required = false) MultipartFile file) throws IOException {
        JSONObject res = new JSONObject();
        JSONObject resUrl = new JSONObject();
        String filename = UUID.randomUUID().toString().replaceAll("-", "");
        String ext = FilenameUtils.getExtension(file.getOriginalFilename());//获得文件扩展名
        String filenames = filename + "." + ext;
        file.transferTo(new File("D:\\chat\\layuichat\\" + filenames));
        resUrl.put("src", "/pic/" + filenames);
        res.put("msg", "");
        res.put("code", 0);
        res.put("data", resUrl);
        return res;
    }
    /**
     * 上传聊天文件
     * **/
    @PostMapping(value = "/chat/upfile")
    @ResponseBody
    public JSONObject upfile(@RequestParam(value = "file", required = false) MultipartFile file) throws IOException {
        JSONObject res = new JSONObject();
        JSONObject resUrl = new JSONObject();
        String filename = UUID.randomUUID().toString().replaceAll("-", "");
        String ext = FilenameUtils.getExtension(file.getOriginalFilename());//获得文件扩展名
        String filenames = filename + "." + ext;
        file.transferTo(new File("D:\\chat\\layuichat\\" + filenames));
        resUrl.put("src", "/pic/" + filenames);
        resUrl.put("name",file.getOriginalFilename());
        res.put("msg", "");
        res.put("code", 0);
        res.put("data", resUrl);
        return res;
    }
    /**
     * 更新签名
     * **/
    @PostMapping(value = "/chat/upsigin")
    @ResponseBody
    public void upsigin(@RequestBody Mine mine){
        mineService.upUserMine(mine);
    }
    /**
     * TODO 跳转到聊天
     * */
    @GetMapping("/layuiim/{userid}")
    public String layuiim(@PathVariable("userid")String userid,HttpSession session){
        session.setAttribute("userid",userid);
        return "/chat/layuiim";
    }
    /**
     * TODO 跳转到聊天记录界面
     * */
    @GetMapping("/tochatlog")
    public String tochatlog(){
        return "/chat/chatlog";
    }
    /**
     * TODO 查询聊天记录
     * */
    @GetMapping("/chatlog/{uid}")
    @ResponseBody
    public List<Mine> chatlog(@PathVariable("uid")String uid,HttpSession session) throws InterruptedException {
        String userid=(String) session.getAttribute("userid");
        Thread.sleep(1000);//模拟消息查询缓慢，让前台展示loading样式
        List<Mine> mines = chatmsgService.LookChatMsg(new Chatmsg().setSenduserid(userid).setReciveuserid(uid));
        return mines;
    }
    /**
     * TODO 初始化聊天
     * */
    @GetMapping("/initim")
    @ResponseBody
    public InitImVo initim(HttpSession session){
        String userid = (String) session.getAttribute("userid");
        InitImVo initImVo=new InitImVo();
        //个人信息
        Mine mine=friendsService.LookUserMine(userid);
        //好友列表
        List<Mine> list=friendsService.LookUserFriend(userid);
        Friend friend=new Friend().setId("2").setGroupname("分组").setList(list);
        List<Friend> friendList=new ArrayList<>();
        friendList.add(friend);
        //群组信息
        List<Groups> groupList=new ArrayList<>();
        groupList.add(groupsService.LookUserGroups(userid));
        //Data数据
        ImData imData=new ImData().setMine(mine).setFriend(friendList).setGroup(groupList);
        initImVo.setCode(0).setMsg("").setData(imData);
        return initImVo;
    }

}
