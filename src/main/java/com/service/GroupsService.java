package com.service;

import com.bean.Groups;
import com.bean.Groupsmsg;
import com.bean.Mine;
import com.mapper.GroupsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupsService {
    @Autowired
    GroupsMapper groupsMapper;
    public Groups LookUserGroups(String userid){
        return groupsMapper.LookUserGroups(userid);
    }
    public List<String> LookGroupUserid(String id){
        return groupsMapper.LookGroupUserid(id);
    }
    @Async
    public void InsertGroupMsg(Groupsmsg groupsmsg){
        groupsMapper.InsertGroupMsg(groupsmsg);
    }
    public List<Mine> LookGroupMsg(String gid){
        return groupsMapper.LookGroupMsg(gid);
    }
}
