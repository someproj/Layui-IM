package com.service;

import com.bean.Mine;
import com.mapper.MineMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MineService {
    @Autowired
    MineMapper mineMapper;
    public void upUserMine(Mine mine){
        mineMapper.upUserMine(mine);
    }
}
